const router = require('express').Router();
const _ = require('lodash');
const ObjectID = require('mongodb').ObjectID;
const validateToken = require('../_helpers/tokenValidator.js').validateToken;

module.exports = (mongoService) => {
    const translationsCollection = mongoService.collection('translations');

    router.get('/search/:name', validateToken, (req, res) => {
        const { name } = req.params;
        if (!name) {
            return res.status(422).json({ errorMessage: 'Invalid name' });
        }
        const query = {
            name: req.sanitize(name).toLowerCase(),
        };

        translationsCollection.find(query).toArray()
            .then(result => res.status(200).json({ translation: result[0] }))
            .catch(() => res.status(500).json({ translation: {}, errorMessage: "Something went wrong while getting translation." }));
    });

    router.get('/search', validateToken, (req, res) => {
        translationsCollection.find().toArray()
            .then((result) => {
                if (result.length > 0) {
                    res.status(200).json({ translations: result });
                } else {
                    res.status(200).json({ translations: {}, errorMessage: "No translations found." });
                }
            })
            .catch(() => res.status(500).json({ translations: {}, errorMessage: "Something went wrong while getting translations." }));
    });

    router.post('/', validateToken, (req, res) => {
        const { translation } = req.body;

        if (translation?.name) {
            return res.status(400).json({ errorMessage: 'Name field is required' });
        }

        Object.keys(translation).forEach((key) => {
            translation[key] = req.sanitize(translation[key]).toLowerCase();
        });

        const query = { translation: translation.name };

        translationsCollection.find(query).toArray()
            .then((result) => {
                if (result.length === 0) {
                    translationsCollection.insert(translation)
                        .then(() => res.status(200).json({ translation, message: 'Translation created successfully.' }));
                } else {
                    res.status(500).json({ errorMessage: 'Translation already exists.' });
                }
            })
            .catch(err => res.status(500).json({ err, errorMessage: 'Something went wrong while addind new translation' }));
    });

    router.put('/', validateToken, (req, res) => {
        const { translation } = req.body;

        if (!translation?.name) {
            return res.status(400).json({ errorMessage: 'Name field is required' })
        }

        const selectBy = {
            name: translation.name.toLowerCase(),
        };

        delete translation._id;

        Object.keys(translation).forEach((key) => {
            translation[key] = req.sanitize(translation[key]).toLowerCase();
        });

        translationsCollection.update(
            selectBy,
            { $set: translation },
        )
            .then(result => res.json(result))
            .catch(err => res.json(err));
    });


    router.delete('/:name', validateToken, (req, res) => {
        const { name } = req.params;

        if (!name) {
            return res.status(422).json({ errorMessage: 'Invalid name' });
        }

        const query = {
            name: req.sanitize(name).toLowerCase(),
        };

        try {
            translationsCollection.deleteOne(query)
                .then(res.status(200).json({ message: 'Translation deleted successfully.' }))
        } catch (e) {
            res.status(500).json({ errorMessage: 'Something went wrong while deleting a translation' });
        }
    });

    return router;
};
